package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods 
{
	public MyLeadsPage() 
	{
		PageFactory.initElements(driver, this);

	}

	//Click CreateLead 
	@FindBy(linkText = "Create Lead")
	WebElement eleCreateLead;
	public MyLeadsPage clickCreateLead() 
	{
		click(eleCreateLead);
		return this;

	}
	//Enter Company Name
	@FindBy(id = "createLeadForm_companyName")
	WebElement eleCName;
	public MyLeadsPage enterCName(String CName)
	{
		type(eleCName, CName);
		return this;

	}
	//Enter First Name
	@FindBy(id = "createLeadForm_firstName")
	WebElement eleFName;
	public MyLeadsPage enterFName(String FName)
	{
		type(eleFName, FName);
		return this;

	}
	//Enter Last Name
	@FindBy(id = "createLeadForm_lastName")
	WebElement eleLName;
	public MyLeadsPage enterLName(String LName)
	{
		type(eleLName, LName);
		return this;

	}
	//Click Submit Button (Create Lead) 
	@FindBy(name = "submitButton")
	WebElement eleClickSubmit;
	public MyLeadsPage clickSubmit() 
	{
		click(eleClickSubmit);
		return this;
	}
}