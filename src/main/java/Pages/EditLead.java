package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods
{
	public EditLead() 
	{
		PageFactory.initElements(driver, this);

	}

	//Click CreateLead 
	@FindBy(linkText = "Find Leads")
	WebElement eleFindLead;
	public EditLead clickFindLeads() 
	{
		click(eleFindLead);
		return this;

	}
	//Enter First Name
	@FindBy(xpath = "(//input [@name = 'firstName'])[3]")
	WebElement eleFName;
	public EditLead enterFName(String FName)
	{
		type(eleFName, FName);
		return this;

	}
	//Click FindLeads Button
	@FindBy(xpath = "//button [text() = 'Find Leads']")
	WebElement eleFindLeadsButton;
	public EditLead findLeadsButton()
	{
		click(eleFindLeadsButton);
		return this;

	}
	//Click First Row
	@FindBy(xpath = "(//a [@class = 'linktext'])[4]")
	WebElement eleFirstlead;
	public EditLead clickFirstRow() throws InterruptedException
	{
		Thread.sleep(3000);
		click(eleFirstlead);
		return this;


	}
	//Click Edit Lead) 
	@FindBy(xpath = "//a[text() = 'Edit']")
	WebElement eleEditbutton;
	public EditLead editLeadButton() 
	{
		click(eleEditbutton);
		return this;
	}
	//Edit Company Name
	@FindBy(xpath = "(//input [@name = 'companyName'])[2]")
	WebElement eleCName;
	public EditLead enterCName(String CName)
	{
		type(eleCName, CName);
		return this;

	}

	//Click Update)
	@FindBy(xpath = "//input [@name = 'submitButton']")
	WebElement eleUpdateButton;
	public EditLead clickUpdateButton() 
	{
		click(eleUpdateButton);
		return this;
	}
}


