package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods
{
	public HomePage() {
		PageFactory.initElements(driver, this);

	}
	//Get text of the logout 
	@FindBy(how = How.XPATH, using = "//div[@id = 'form']/h2")
	WebElement eleName;
	public HomePage LoginName(String expectedText) 
	{
		verifyPartialText(eleName, expectedText);
		return this;

	}
	//Click CRM/SFA 
	@FindBy(linkText = "CRM/SFA")
	WebElement eleCrmSfa;
	public MyHomePage clickCrmSfa() 
	{
		click(eleCrmSfa);
		return new MyHomePage();

	}
}