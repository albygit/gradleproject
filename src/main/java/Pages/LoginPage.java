package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods
{
	public LoginPage() {
		PageFactory.initElements(driver, this);

	}
	//Enter UserName	
	@FindBy(id = "username")
	WebElement eleUserName;
	public LoginPage typeUserName(String data) 
	{
		type(eleUserName, data);
		return this;

	}
	//Enter Password
	@FindBy(id = "password")
	WebElement elePassword;
	public LoginPage typePassword(String data) 
	{
		type (elePassword, data);
		return this;

	}
	//Click Login
	@FindBy(how = How.CLASS_NAME, using= "decorativeSubmit")
	WebElement eleLogin;
	public HomePage clicklogin() {
		click(eleLogin);
		return new HomePage();
	}
}