package Week4.Day2;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class EditLeadSE extends ProjectMethods
{
		@BeforeTest
		public void setData() {
			testCaseName = "TC002_EditLead";
			testCaseDesc = "Edit a Lead with SE methods";
			category = "Automated";
			author = "Alban";
		}

		//login is already called from ProjectMethods BeforeMethod
        @Test
		public void EditLead() throws InterruptedException 
        {
        	//Click Leads link
        	locateElement("linkText", "Leads").click();
        	//Click Find leads
        	locateElement("linkText", "Find Leads").click();
        	//Enter first name
        	locateElement("xpath", "(//input [@name = 'firstName'])[3]").sendKeys("Alban");
        	//Click Find leads button
        	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        	//Click on first resulting lead
        	Thread.sleep(3000);
            locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();
            //Verify title of the page
        	getTitle();
        	//Click Edit
        	locateElement("xpath", "//a[text() = 'Edit']").click();
        	//Change the company name
        	locateElement("xpath", "(//input [@name = 'companyName'])[2]").clear();
           	locateElement("xpath", "(//input [@name = 'companyName'])[2]").sendKeys("DXC Technology");
        	//Click Update
        	locateElement("xpath", "//input [@name = 'submitButton']").click();
        	//Confirm the changed name appears
        	String text = locateElement("xpath", "//span [@id = 'viewLead_companyName_sp']").getText();
        	reportStep("The data: "+text+" entered successfully","Pass");
        	        	
        }


}
