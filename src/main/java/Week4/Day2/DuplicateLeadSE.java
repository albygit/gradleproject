package Week4.Day2;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DuplicateLeadSE extends ProjectMethods
{
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC004_DuplicateLead";
		testCaseDesc = "Duplicate Lead with SE methods";
		category = "Automated";
		author = "Alban";
	}

	//login is already called from ProjectMethods BeforeMethod
    @Test
	public void DeleteLead() throws InterruptedException 
    {
    	//Click Leads link
    	locateElement("linkText", "Leads").click();
    	//Click Find leads
    	locateElement("linkText", "Find Leads").click();
       	//Click on Email
    	locateElement("xpath", "//span [text() = 'Email']").click();
    	        	
    	//Enter Email
    	locateElement("xpath", "//input [@name = 'emailAddress']").clear();
    	locateElement("xpath", "//input [@name = 'emailAddress']").sendKeys("al123@dxc.com");
    	   	
     	//Click find leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        //Capture lead ID of First Resulting lead
    	Thread.sleep(3000);
        String text2 = locateElement("xpath", "(//a [@class = 'linktext'])[4]").getText();
        reportStep("The data: "+text2+" is clicked successfully","Pass");
        //Click First Resulting lead
        locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();            
        //Click Duplicate Lead
        Thread.sleep(3000);
        locateElement("xpath", "//a[text() = 'Duplicate Lead']").click();	
        //Verify the title as 'Duplicate Lead'
        getTitle();
        //Click Create Lead
    	locateElement("xpath", "//input [@name = 'submitButton']").click();
    	//Confirm the duplicated lead name is same as captured name
    	String text = locateElement("xpath", "//span [@id = 'viewLead_companyName_sp']").getText();
    	reportStep("The data: "+text+" duplicated successfully","Pass");
    	   	
    	    	            
       
    }


}
