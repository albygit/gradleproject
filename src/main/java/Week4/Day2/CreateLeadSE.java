package Week4.Day2;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class CreateLeadSE extends ProjectMethods
{
		@BeforeTest
		public void setData() {
			testCaseName = "CreateLeadSE";
			testCaseDesc = "Create a Lead with SE methods";
			category = "Automated";
			author = "Alban";
		}
		@Test
		public void CreateLead() throws InterruptedException 
        {
	
			//select CRM/SFA applicaation
			   driver.findElementByLinkText("CRM/SFA").click();
			   driver.findElementByLinkText("Create Lead").click();
			   driver.findElementById("createLeadForm_companyName").sendKeys("DXC Technology");
			   driver.findElementById("createLeadForm_firstName").sendKeys("Alban");
			   driver.findElementById("createLeadForm_lastName").sendKeys("Inbaraj");
			   driver.findElementById("createLeadForm_firstNameLocal").sendKeys("albie");
			   driver.findElementById("createLeadForm_lastNameLocal").sendKeys("itme");
			   driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");	
			
			
			
        }
}
