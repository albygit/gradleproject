package Week4.Day2;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLeadSE extends ProjectMethods
{
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testCaseDesc = "Merge a Lead with SE methods";
		category = "Automated";
		author = "Alban";
	}

	//login is already called from ProjectMethods BeforeMethod
    @Test
	public void MergeLead() throws InterruptedException 
    {
    	//Click Leads link
    	locateElement("linkText", "Leads").click();
       	//Click Merge leads
    	locateElement("linkText", "Merge Leads").click();
       	//Click on Icon near From Lead
    	locateElement("xpath", "//img [@alt = 'Lookup']").click();
    	//Move to new window
    	switchToWindow(1);
    	//Enter Lead ID
    	locateElement("xpath", "//input [@name = 'id']").sendKeys("10384");
       	//Click Find Leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        //Click First Resulting lead
    	Thread.sleep(3000);
    	String text = locateElement("xpath", "//a [@class = 'linktext']").getText();
    	locateElement("xpath", "//a [@class = 'linktext']").click();
    	//Move to new window
    	switchToWindow(0);
    	//Click on Icon near To Lead
    	locateElement("xpath", "(//img [@alt = 'Lookup'])[2]").click();
    	//Move to new window
    	switchToWindow(1);
    	//Enter Lead ID
    	locateElement("xpath", "//input [@name = 'id']").sendKeys("10387");
    	//Click Find Leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
    	//Click First Resulting lead
    	Thread.sleep(3000);
    	locateElement("xpath", "//a [@class = 'linktext']").click();
    	//Move to new window
    	switchToWindow(0);
    	//Click Merge
    	locateElement("xpath", "//a[text() = 'Merge']").click();
    	//Click Alert
    	Thread.sleep(3000);
    	acceptAlert();
    	//Click Find Leads
    	locateElement("xpath", "//a[text() = 'Find Leads']").click();
    	//Enter From Lead ID
    	locateElement("xpath", "(//input [@name = 'id'])").sendKeys(text);
    	//Click Find leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
    	//Verify error msg
    	String text1 = locateElement("xpath", "//div [text() = 'No records to display']").getText();
    	reportStep("The message is : "+text1+" ","Pass");
    	        	    	
       
    }

	
}
