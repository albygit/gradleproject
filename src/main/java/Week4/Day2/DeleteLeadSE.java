package Week4.Day2;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DeleteLeadSE extends ProjectMethods

{
		@BeforeTest
		public void setData() {
			testCaseName = "TC003_DeleteLead";
			testCaseDesc = "Delete a Lead with SE methods";
			category = "Automated";
			author = "Alban";
		}

		//login is already called from ProjectMethods BeforeMethod
        @Test
		public void DeleteLead() throws InterruptedException 
        {
        	//Click Leads link
        	locateElement("linkText", "Leads").click();
        	//Click Find leads
        	locateElement("linkText", "Find Leads").click();
           	//Click on Phone
        	locateElement("xpath", "//span [text() = 'Phone']").click();
        	        	
        	//Enter phone number
        	locateElement("xpath", "//input [@name = 'phoneCountryCode']").clear();
        	locateElement("xpath", "//input [@name = 'phoneCountryCode']").sendKeys("1+44");
        	locateElement("xpath", "//input [@name = 'phoneAreaCode']").sendKeys(Keys.TAB);
        	locateElement("xpath", "//input [@name = 'phoneAreaCode']").sendKeys("743");
        	locateElement("xpath", "//input [@name = 'phoneNumber']").sendKeys(Keys.TAB);
        	locateElement("xpath", "//input [@name = 'phoneNumber']").sendKeys("745-568-8899");
        	//locateElement("xpath", "//input [@name = 'primaryPhoneNumber']").sendKeys(Keys.TAB);
        	
        	//Click find leads button
        	locateElement("xpath", "//button [text() = 'Find Leads']").click();
            //Capture lead ID of First Resulting lead
        	Thread.sleep(3000);
            String text2 = locateElement("xpath", "(//a [@class = 'linktext'])[4]").getText();
            reportStep("The data: "+text2+" entered successfully","Pass");
            //Click First Resulting lead
            locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();
            //Click Delete
            Thread.sleep(3000);
            locateElement("xpath", "//a[text() = 'Delete']").click();	
            //Click Find leads
        	locateElement("linkText", "Find Leads").click();
        	//Enter captured lead ID
        	locateElement("xpath", "(//input [@name = 'id'])").sendKeys(text2);
        	//Click Find leads button
        	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        	//Verify error msg
        	String text3 = locateElement("xpath", "//div [text() = 'No records to display']").getText();
        	reportStep("The message is : "+text3+" ","Pass");
        	            
           
        }

}


