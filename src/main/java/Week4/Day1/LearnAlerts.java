package Week4.Day1;

//import java.util.List;
import java.util.concurrent.TimeUnit;

//import org.apache.poi.util.SystemOutLogger;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlerts {

	public static void main(String[] args)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	       ChromeDriver driver = new ChromeDriver();
	       //maximize the window
		   driver.manage().window().maximize();
	           
			//Load URL
			   driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
			   //Implicit Wait
			   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			  // int s = driver.findElements(By.tagName("iframe")).size();
			   //System.out.println(s)
			   WebElement frame = driver.findElementById("iframeResult");
              driver.switchTo().frame(frame);        
              driver.findElementByXPath("//button [text() = 'Try it']").click();
              
              //Send value to the alert (text box)
              driver.switchTo().alert().sendKeys("Alban");
             //accept an alert
              driver.switchTo().alert().accept();
              //get the text from frame and print
              String text = driver.findElementByXPath("//p[@id ='demo']").getText();
              System.out.println(text);
                           
              
     	}

}
