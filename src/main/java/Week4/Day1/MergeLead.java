package Week4.Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead 
{
	public static void main(String[] args)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	       ChromeDriver driver = new ChromeDriver();
	       //maximize the window
		   driver.manage().window().maximize();
	           
			//Load URL
			   driver.get("http://leaftaps.com/opentaps/control/main");
			   //Implicit Wait
			   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			   
			 //Find the elements 
			   driver.findElementById("username").sendKeys("DemoSalesManager");
			   driver.findElementById("password").sendKeys("crmsfa");
			   driver.findElementByClassName("decorativeSubmit").click();
			   
			 //select CRM/SFA applicaation
			   driver.findElementByLinkText("CRM/SFA").click();
			   driver.findElementByLinkText("Create Lead").click();
			   driver.findElementByLinkText("Merge Leads").click();
			   driver.findElementByXPath("//table[@class ='twoColumnForm']/tbody[18]/td[2]/a ").click();
			   
	}

}
