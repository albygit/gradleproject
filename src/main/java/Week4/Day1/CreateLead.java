package Week4.Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class CreateLead {
	
	public static void main(String[] args)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	       ChromeDriver driver = new ChromeDriver();
	       //maximize the window
		   driver.manage().window().maximize();
	           
			//Load URL
			   driver.get("http://leaftaps.com/opentaps/control/main");
			   //Implicit Wait
			   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			   
			 //Find the elements 
			   driver.findElementById("username").sendKeys("DemoSalesManager");
			   driver.findElementById("password").sendKeys("crmsfa");
			   driver.findElementByClassName("decorativeSubmit").click();
			   
			   //select CRM/SFA applicaation
			   driver.findElementByLinkText("CRM/SFA").click();
			   driver.findElementByLinkText("Create Lead").click();
			   driver.findElementById("createLeadForm_companyName").sendKeys("DXC Technology");
			   driver.findElementById("createLeadForm_firstName").sendKeys("Alban");
			   driver.findElementById("createLeadForm_lastName").sendKeys("Inbaraj");
			   driver.findElementById("createLeadForm_firstNameLocal").sendKeys("albie");
			   driver.findElementById("createLeadForm_lastNameLocal").sendKeys("itme");
			   driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
			   //select dropdown source
			   WebElement sourcedropdown = driver.findElementById("createLeadForm_dataSourceId");
			   Select dropdown = new Select (sourcedropdown);
			   dropdown.selectByVisibleText("Cold Call");
			   driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Lead");
			   driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000000");
			   //select dropdown Industry
			   WebElement inddropdown = driver.findElementById("createLeadForm_industryEnumId");
			   Select dropdown1 = new Select (inddropdown);
			   dropdown1.selectByVisibleText("Computer Software");
			   //select dropdown ownership 
			   WebElement owndropdown = driver.findElementById("createLeadForm_ownershipEnumId");
			   Select dropdown2 = new Select (owndropdown);
			   dropdown2.selectByVisibleText("Partnership");
			   driver.findElementById("createLeadForm_sicCode").sendKeys("47310");
			   driver.findElementById("createLeadForm_description").sendKeys("Retail sale of computers, peripheral units, software and telecommunications");
			   driver.findElementById("createLeadForm_importantNote").sendKeys("software details");
			   driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("+44");
			   driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("743");
			   driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("714317");
			   driver.findElementById("createLeadForm_departmentName").sendKeys("Computer Department");
			  //select dropdown currency 
			   WebElement curdropdown = driver.findElementById("createLeadForm_currencyUomId");
			   Select dropdown3 = new Select (curdropdown);
			   dropdown3.selectByVisibleText("USD - American Dollar");
			   driver.findElementById("createLeadForm_numberEmployees").sendKeys("10");
			   driver.findElementById("createLeadForm_tickerSymbol").sendKeys("DXC LTD");
			   driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("943332455");
			   driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.dxc.com");
			   driver.findElementById("createLeadForm_generalToName").sendKeys("Inbaraj");
			   driver.findElementById("createLeadForm_generalAddress1").sendKeys("Address 1");	
			   driver.findElementById("createLeadForm_generalAddress2").sendKeys("Address 2");
			   driver.findElementById("createLeadForm_generalCity").sendKeys("Watford");
			   //select dropdown state/province 
			   WebElement statedropdown = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
			   Select dropdown4 = new Select (statedropdown);
			   dropdown4.selectByVisibleText("Alabama");
			  //select dropdown country 
			   WebElement condropdown = driver.findElementById("createLeadForm_generalCountryGeoId");
			   Select dropdown5 = new Select (condropdown);
			   dropdown5.selectByVisibleText("United States");
			   driver.findElementById("createLeadForm_generalPostalCode").sendKeys("35010");
			   driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("055");	
			   //select dropdown Mar campaing 
			   WebElement mardropdown = driver.findElementById("createLeadForm_marketingCampaignId");
			   Select dropdown6 = new Select (mardropdown);
			   dropdown6.selectByVisibleText("Automobile");
			   driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("745-568-8899");
			   driver.findElementById("createLeadForm_primaryEmail").sendKeys("al123@dxc.com");
			   //click create lead
			          
			   driver.findElementByName("	").click();
			   			   
	    //Verify the 'first name' 
			   String text = driver.findElementById("viewLead_firstName_sp").getText();
			   System.out.println("The First name is :"  +text);
	}

}
