package Week4.Day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWinHandle 
{

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	       ChromeDriver driver = new ChromeDriver();
	       //maximize the window
		   driver.manage().window().maximize();
	           
			//Load URL
			   driver.get("https://www.irctc.co.in/nget/train-search");
			   //Implicit Wait
			   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			   driver.findElementByXPath("//span [text() = 'AGENT LOGIN']").click();
			   driver.findElementByLinkText("Contact Us").click();
			   //get the new window properties
			   Set<String> windowHandles = driver.getWindowHandles();
			   List <String> listofwindows = new ArrayList<>();
			   listofwindows.addAll(windowHandles);
			   driver.switchTo().window(listofwindows.get(1));
			   System.out.println(driver.getTitle());
			   
			   
			   
			   
			   

	}
}