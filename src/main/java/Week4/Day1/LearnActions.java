package Week4.Day1;

import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnActions {

	public static void main(String[] args)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	       ChromeDriver driver = new ChromeDriver();
	       //maximize the window
		   driver.manage().window().maximize();
	           
			//Load URL
			   driver.get("http://jqueryui.com");
			   driver.findElementByLinkText("Droppable").click();
			   //Implicit Wait
			   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			   //loacting frame
			   
			   //import Actions for mouse and keyboard options
			   Actions builder = new Actions(driver);
			   //find the frame
			   WebElement frame = driver.findElementByClassName("demo-frame");
			   driver.switchTo().frame(frame);
			   WebElement drag = driver.findElementById("draggable");
			   System.out.println(drag.getLocation());
			   builder.dragAndDropBy(drag, 50, 128).perform();
			   System.out.println("The frame is moved");
			   
			
	}

}
