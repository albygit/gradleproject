package Week2.Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListArray
{

	public static void main(String[] args)
	{
	List<String>myPhones = new ArrayList<String>();
	myPhones.add("OnePlus");
	myPhones.add("Nokia");
	myPhones.add("Sony");
	myPhones.add("HTC");
	
	//1)Should have all mobile phones Listed use - for each -
	for (String eachphone : myPhones )
	{
	System.out.println(eachphone);
	}
	
	//2)Find the count of the mobile names - use size
	myPhones.size();
	int size = myPhones.size();
	System.out.println("The count of the mobile phones: " +size);
	
	//3)Print the last but one before from mobile names - use get	
	System.out.println("The last but one before mobile phones: "+myPhones.get(size-2));
	
	//4)Print the list of mobile in ASCII order - use collections 
	Collections.sort(myPhones); //this command will sort the array with the ASCII values
	for (String eachphone : myPhones ) //to print the sorted - use for each
	{
	System.out.println(eachphone);
	}
	
	
	

}
}
