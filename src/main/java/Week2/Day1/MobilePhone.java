package Week2.Day1;

public class MobilePhone extends TeleCom implements InterfaceMobilePhone, InterfaceSimRules 
{

	 public void sendSMS() 
	 {
		System.out.println("sendSMS"); 
	 }
	 public void sendWatsapp() 
	 {
		System.out.println("sendWatsapp"); 
	 }	
	 //overriding
	 public void ringTone()
	{
			System.out.println("Myphone RingTone"); 
				
	}
	@Override
	public void support4G() {
		System.out.println("Support 4G"); 
		
	}
	@Override
	public void supportSIM() {
		System.out.println("supportSIM"); 
		
	} 
	 
	
}
