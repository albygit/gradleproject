package Week2.Day2;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
//import java.util.TreeSet;

public class DulMobile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        //1)Print all the mobile without duplicates
		Set<String> mobiles = new LinkedHashSet<String>();
		mobiles.add("Nokia");
		mobiles.add("Moto");
		mobiles.add("Samsung");
		mobiles.add("IPhone");
		mobiles.add("Nokia");
		System.out.println(mobiles);
		
		//2)Print the first mobile from the list
		List<String> lst = new ArrayList<String>();
		lst.addAll(mobiles);
		System.out.println("First mobile from the list: " +lst.get(3));
		
	}

}
