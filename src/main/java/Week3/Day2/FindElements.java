package Week3.Day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElements {

	public static void main(String[] args)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	       ChromeDriver driver = new ChromeDriver();
	       //maximize the window
		   driver.manage().window().maximize();
	           
			//Load URL
			   driver.get("http://leafground.com/pages/table.html");
			   //Implicit Wait
			   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			 
			 //find the list of check boxed and click on 3rd box  
			 List <WebElement> box = driver.findElementsByXPath("//input [@type = 'checkbox']");
			 WebElement thrid = box.get(2);
			 thrid.click();
			 
			 //check the box with 80%
			WebElement table = driver.findElementByXPath("//tr [@class ='even']");
			List<WebElement> tableRow = table.findElements(By.tagName("td"));
			System.out.println(tableRow.size());
			
			
			
			
			
			
			 
	
	}
}
