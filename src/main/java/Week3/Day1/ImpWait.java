package Week3.Day1;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ImpWait {

	public static void main(String[] args)
		{
		// TODO Auto-generated method stub
       System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
       ChromeDriver driver = new ChromeDriver();
       //maximize the window
	   driver.manage().window().maximize();
       
       try {
		//Load URL
		   driver.get("	");
		   //Implicit Wait
		   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		   //Find the elements 
		   driver.findElementById("username").sendKeys("DemoSalesManager");
		   driver.findElementById("password").sendKeys("crmsfa");
		   driver.findElementByClassName("decorativeSubmit").click();
		   
		   //select CRM/SFA applicaation
		   driver.findElementByLinkText("CRM/SFA").click();
		   driver.findElementByLinkText("Create Lead").click();
		   driver.findElementById("createLeadForm_companyName").sendKeys("DXC Technology");
		   driver.findElementById("createLeadForm_firstName").sendKeys("Alban");
		   driver.findElementById("createLeadForm_lastName").sendKeys("Inbaraj");
		   
		   //select value from 'Source Dropdown values - using 'visible text' method
		   WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		   Select dropDown = new Select(src);
		   dropDown.selectByVisibleText("Direct Mail");
		   
		 //select value from 'Marketing Campaign Dropdown values - using 'index' method 
		   WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
		   Select dropDown1 = new Select(src1);
		   List<WebElement> value = dropDown1.getOptions();
		    dropDown1.selectByIndex(value.size()-1);
		  
		 driver.findElementByName("submitButton").click();
		   
       }
       catch (NoSuchElementException e)
       {
				e.printStackTrace();
				System.out.println("NoSuchElementException Found");
	   }
       
       
     finally {
    	 driver.close();
     }
       
       
                    
	}
	 
}
