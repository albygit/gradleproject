package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String filename) throws IOException
	{
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		//Get sheet of index 0
		XSSFSheet sheet = wbook.getSheetAt(0);
		//to get the last row in a sheeet
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row count ="+rowCount);
        int columnCount = sheet.getRow(0).getLastCellNum();
        System.out.println("Column count = "+columnCount);
       //put the coloumn and row in the group
       Object[][] data = new Object[rowCount][columnCount];
        for (int j =1; j<=rowCount; j++)
        {
        	XSSFRow row = sheet.getRow(j);
        	for (int i =0; i< columnCount; i++)
        	{
        	  XSSFCell cell = row.getCell(i);
        	 data[j-1][i] = cell.getStringCellValue();
        	  System.out.println(data);
           	}
        	
        }
		return data;
	}

}
