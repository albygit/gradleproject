package ProjectDay;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar 
{

	public static void main(String[] args) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver = new ChromeDriver();
	    //maximize the window
		driver.manage().window().maximize();
		
		//Load URL
		driver.get("https://www.zoomcar.com/chennai");
		//Implicit Wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		// Click on the Start your wonderful journey link
		driver.findElementByLinkText("Start your wonderful journey").click();
		
		//In the Search page, Click on the any of the pick up point under POPULAR PICK-UP POINTS
		Thread.sleep(3000);
		driver.findElementByXPath("//div [@class = 'items'][1]").click();
		
		//Click on the Next button
		driver.findElementByXPath("//button [text() = 'Next']").click();
		
		//Specify the Start Date as tomorrow Date
		// Get the current date
				Date date = new Date();
		// Get only the date (and not month, year, time etc)
				DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
				String today = sdf.format(date);
		// Convert to integer and add 1 to it
				int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
				System.out.println(tomorrow);
		
		driver.findElementByXPath("//div [@class = 'day'][1]").click();
		
		// Click on the Next Button
		driver.findElementByXPath("//button [text() = 'Next']").click();
		
		//  Confirm the Start Date and Click on the Done button
		driver.findElementByXPath("//button [text() = 'Done']").click();
		
		// In the result page, capture the number of results displayed
		List<WebElement> listofitems = driver.findElementsByXPath("//div [@class = 'img']");
		System.out.println(listofitems.size());
		
		//Find the highest value and report the brand name
		List<WebElement> Price = driver.findElementsByXPath("//div [@class ='price']");
		List<String> list = new ArrayList<>();
		for (WebElement eachPrice : Price )
		{
		System.out.println(list.add(eachPrice.getText().replaceAll("//D", "").replaceAll("[₹  ]", "")));
		
		}
		String max = Collections.max(list);
	 	System.out.println("The maximum value :" +max);
	 	
	 	Thread.sleep(3000);
	 	String text = driver.findElementByXPath("//div [contains (text(),"+max+")]/../../../div[2]/h3").getText();
	 	System.out.println(text);
	 	driver.findElementByXPath("//div [contains (text(),"+max+")]/following::button").click();
	 	driver.close();
	}
	
	
	   
}

