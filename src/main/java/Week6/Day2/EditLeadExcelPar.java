package Week6.Day2;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class EditLeadExcelPar extends ProjectMethods
{
		@BeforeTest(groups = "common")
		public void setData() {
			testCaseName = "EditLeadExcelPar";
			testCaseDesc = "Edit a Lead with SE methods";
			category = "Automated";
			author = "Alban";
			excelFileName = "EL";
					
		}

		//login is already called from ProjectMethods BeforeMethod
        @Test(groups = "smoke", dataProvider = "fetchdata")
		public void EditLead(String firstName, String companyName ) throws InterruptedException 
        {
        	//Click Leads link
        	locateElement("linkText", "Leads").click();
        	//Click Find leads
        	locateElement("linkText", "Find Leads").click();
        	//Enter first name
        	locateElement("xpath", "(//input [@name = 'firstName'])[3]").sendKeys(firstName);
        	//Click Find leads button
        	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        	//Click on first resulting lead
        	Thread.sleep(3000);
            locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();
            //Verify title of the page
        	getTitle();
        	//Click Edit
        	locateElement("xpath", "	").click();
        	//Change the company name
        	locateElement("xpath", "(//input [@name = 'companyName'])[2]").clear();
           	locateElement("xpath", "(//input [@name = 'companyName'])[2]").sendKeys(companyName);
        	//Click Update
        	locateElement("xpath", "//input [@name = 'submitButton']").click();
        	//Confirm the changed name appears
        	String text = locateElement("xpath", "//span [@id = 'viewLead_companyName_sp']").getText();
        	reportStep("The data: "+text+" entered successfully","Pass");
        	        	
        }
       
        
}
