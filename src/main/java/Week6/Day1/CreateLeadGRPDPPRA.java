package Week6.Day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class CreateLeadGRPDPPRA extends ProjectMethods
{
		
		@BeforeTest
		public void setData() {
			testCaseName = "CreateLeadGRPDPPRA";
			testCaseDesc = "Create a Lead with SE methods, Group, Parameters";
			category = "Automated";
			author = "Alban";
		}
		@Test(groups = "sanity", dataProvider = "Create")
		public void CreateLead(String companyName,String firstName, String lastName) throws InterruptedException 
        {
	
			 //select CRM/SFA applicaation
			   //locateElement("linkTest", "CRM/SFA").click();
			   locateElement("linkText","Create Lead").click();
			   locateElement("id","createLeadForm_companyName").sendKeys(companyName);
			   locateElement("id","createLeadForm_firstName").sendKeys(firstName);
			   locateElement("id","createLeadForm_lastName").sendKeys(lastName);
			   locateElement("id","createLeadForm_firstNameLocal").sendKeys("albie");
			   locateElement("id","createLeadForm_lastNameLocal").sendKeys("itme");
			   locateElement("id","createLeadForm_personalTitle").sendKeys("Mr");	
			 //select dropdown source
			   WebElement sourcedropdown = driver.findElementById("createLeadForm_dataSourceId");
			   Select dropdown = new Select (sourcedropdown);
			   dropdown.selectByVisibleText("Cold Call");
			   locateElement("id","createLeadForm_generalProfTitle").sendKeys("Lead");
			   locateElement("id","createLeadForm_annualRevenue").sendKeys("1000000");
			 //select dropdown Industry
			   WebElement inddropdown = driver.findElementById("createLeadForm_industryEnumId");
			   Select dropdown1 = new Select (inddropdown);
			   dropdown1.selectByVisibleText("Computer Software");
			 //select dropdown ownership 
			   WebElement owndropdown = driver.findElementById("createLeadForm_ownershipEnumId");
			   Select dropdown2 = new Select (owndropdown);
			   dropdown2.selectByVisibleText("Partnership");
			   locateElement("id","createLeadForm_sicCode").sendKeys("47310");
			   locateElement("id","createLeadForm_description").sendKeys("Retail sale of computers, peripheral units, software and telecommunications");
			   locateElement("id","createLeadForm_importantNote").sendKeys("software details");
			   locateElement("id","createLeadForm_primaryPhoneCountryCode").sendKeys("+44");
			   locateElement("id","createLeadForm_primaryPhoneAreaCode").sendKeys("743");
			   locateElement("id","createLeadForm_primaryPhoneExtension").sendKeys("714317");
			   locateElement("id","createLeadForm_departmentName").sendKeys("Computer Department");
			 //select dropdown currency 
			   WebElement curdropdown = driver.findElementById("createLeadForm_currencyUomId");
			   Select dropdown3 = new Select (curdropdown);
			   dropdown3.selectByVisibleText("USD - American Dollar");
			   locateElement("id","createLeadForm_numberEmployees").sendKeys("10");
			   locateElement("id","createLeadForm_tickerSymbol").sendKeys("DXC LTD");
			   locateElement("id","createLeadForm_primaryPhoneAskForName").sendKeys("943332455");
			   locateElement("id","createLeadForm_primaryWebUrl").sendKeys("www.dxc.com");
			   locateElement("id","createLeadForm_generalToName").sendKeys("Inbaraj");
			   locateElement("id","createLeadForm_generalAddress1").sendKeys("Address 1");	
			   locateElement("id","createLeadForm_generalAddress2").sendKeys("Address 2");
			   locateElement("id","createLeadForm_generalCity").sendKeys("Watford");
			 //select dropdown state/province 
			   WebElement statedropdown = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
			   Select dropdown4 = new Select (statedropdown);
			   dropdown4.selectByVisibleText("Alabama");
			 //select dropdown country 
			   WebElement condropdown = driver.findElementById("createLeadForm_generalCountryGeoId");
			   Select dropdown5 = new Select (condropdown);
			   dropdown5.selectByVisibleText("United States");
			   locateElement("id","createLeadForm_generalPostalCode").sendKeys("35010");
			   locateElement("id","createLeadForm_generalPostalCodeExt").sendKeys("055");
			 //select dropdown Mar campaing 
			   WebElement mardropdown = driver.findElementById("createLeadForm_marketingCampaignId");
			   Select dropdown6 = new Select (mardropdown);
			   dropdown6.selectByVisibleText("Automobile");
			   locateElement("id","createLeadForm_primaryPhoneNumber").sendKeys("745-568-8899");
			   locateElement("id","createLeadForm_primaryEmail").sendKeys("al123@dxc.com");
			 //click create lead
			   locateElement("name","submitButton").click(); 
			   
			     			
        }
		@DataProvider(name = "Create")
        public Object [][] fetchData()
        {
            Object [][] data = new Object [3][3];
        	data[0][0] = "DXC";
        	data[0][1] = "Alban";
        	data[0][2] = "Inbaraj";
        	
        	data[1][0] = "Accenture";
        	data[1][1] = "Geethanjali";
        	data[1][2] = "Alban";
        	
        	data[2][0] = "CTS";
        	data[2][1] = "Sarah";
        	data[2][2] = "Alban";
        	
        	return data;
		
}
}
