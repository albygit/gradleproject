package Week6.Day1;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DeleteLeadGRPDPPRA extends ProjectMethods

{
		@BeforeTest(groups = "common")
		public void setData() {
			testCaseName = "DeleteLeadGRPDP";
			testCaseDesc = "Delete a Lead with Group,Parameter and Dataprovider";
			category = "Automated";
			author = "Alban";
		}

		//login is already called from ProjectMethods BeforeMethod
        @Test(groups = "sanity", dataProvider = "Delete")
		public void DeleteLead(String phoneCountryCode, String phoneAreaCode, String phoneNumber ) throws InterruptedException 
        {
        	//Click Leads link
        	locateElement("linkText", "Leads").click();
        	//Click Find leads
        	locateElement("linkText", "Find Leads").click();
           	//Click on Phone
        	locateElement("xpath", "//span [text() = 'Phone']").click();
        	        	
        	//Enter phone number
        	locateElement("xpath", "//input [@name = 'phoneCountryCode']").clear();
        	locateElement("xpath", "//input [@name = 'phoneCountryCode']").sendKeys(phoneCountryCode);
        	locateElement("xpath", "//input [@name = 'phoneAreaCode']").sendKeys(Keys.TAB);
        	locateElement("xpath", "//input [@name = 'phoneAreaCode']").sendKeys(phoneAreaCode);
        	locateElement("xpath", "//input [@name = 'phoneNumber']").sendKeys(Keys.TAB);
        	locateElement("xpath", "//input [@name = 'phoneNumber']").sendKeys(phoneNumber);
        	//locateElement("xpath", "//input [@name = 'primaryPhoneNumber']").sendKeys(Keys.TAB);
        	
        	//Click find leads button
        	locateElement("xpath", "//button [text() = 'Find Leads']").click();
            //Capture lead ID of First Resulting lead
        	Thread.sleep(3000);
            String text2 = locateElement("xpath", "(//a [@class = 'linktext'])[4]").getText();
            reportStep("The data: "+text2+" entered successfully","Pass");
            //Click First Resulting lead
            locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();
            //Click Delete
            Thread.sleep(3000);
            locateElement("xpath", "//a[text() = 'Delete']").click();	
            //Click Find leads
        	locateElement("linkText", "Find Leads").click();
        	//Enter captured lead ID
        	locateElement("xpath", "(//input [@name = 'id'])").sendKeys(text2);
        	//Click Find leads button
        	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        	//Verify error msg
        	String text3 = locateElement("xpath", "//div [text() = 'No records to display']").getText();
        	reportStep("The message is : "+text3+" ","Pass");
        	            
           
        }
        @DataProvider(name = "Delete")
        public Object [][] fetchData()
        {
            Object [][] data = new Object [2][3];
        	data[0][0] = "91";
        	data[0][1] = "944";
        	data[0][2] = "3824687";
        	
        	data[1][0] = "91";
        	data[1][1] = "948";
        	data[1][2] = "6113108";
        	return data;
         }

}


