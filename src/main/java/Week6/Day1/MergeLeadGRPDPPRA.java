package Week6.Day1;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLeadGRPDPPRA extends ProjectMethods
{
	@BeforeTest(groups = "common")
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testCaseDesc = "Merge a Lead with SE methods";
		category = "Automated";
		author = "Alban";
	}

	//login is already called from ProjectMethods BeforeMethod
    @Test(groups = "regression", dataProvider = "Merge")
	public void MergeLead(String Lead1, String Lead2) throws InterruptedException 
    {
    	//Click Leads link
    	locateElement("linkText", "Leads").click();
       	//Click Merge leads
    	locateElement("linkText", "Merge Leads").click();
       	//Click on Icon near From Lead
    	locateElement("xpath", "//img [@alt = 'Lookup']").click();
    	//Move to new window
    	switchToWindow(1);
    	//Enter Lead ID
    	locateElement("xpath", "//input [@name = 'id']").sendKeys(Lead1);
       	//Click Find Leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        //Click First Resulting lead
    	Thread.sleep(3000);
    	String text = locateElement("xpath", "//a [@class = 'linktext']").getText();
    	locateElement("xpath", "//a [@class = 'linktext']").click();
    	//Move to new window
    	switchToWindow(0);
    	//Click on Icon near To Lead
    	locateElement("xpath", "(//img [@alt = 'Lookup'])[2]").click();
    	//Move to new window
    	switchToWindow(1);
    	//Enter Lead ID
    	locateElement("xpath", "//input [@name = 'id']").sendKeys(Lead2);
    	//Click Find Leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
    	//Click First Resulting lead
    	Thread.sleep(3000);
    	locateElement("xpath", "//a [@class = 'linktext']").click();
    	//Move to new window
    	switchToWindow(0);
    	//Click Merge
    	locateElement("xpath", "//a[text() = 'Merge']").click();
    	//Click Alert
    	Thread.sleep(3000);
    	acceptAlert();
    	//Click Find Leads
    	locateElement("xpath", "//a[text() = 'Find Leads']").click();
    	//Enter From Lead ID
    	locateElement("xpath", "(//input [@name = 'id'])").sendKeys(text);
    	//Click Find leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
    	//Verify error msg
    	String text1 = locateElement("xpath", "//div [text() = 'No records to display']").getText();
    	reportStep("The message is : "+text1+" ","Pass");
    	        	    	
       
    }
    @DataProvider(name = "Merge")
    public Object [][] fetchData()
    {
        Object [][] data = new Object [2][2];
    	data[0][0] = "10123";
    	data[0][1] = "10124";
    	
    	data[1][0] = "10127";
    	data[1][1] = "10128";
    	
    	return data;
     }
	
}
