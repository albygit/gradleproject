package Week6.Day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;


public class LearnTestNGAttributes extends ProjectMethods
{
	
	@BeforeTest
	public void setData() 
	{
		testCaseName = "LearnTestNGAttributes";
		testCaseDesc = "Edit a Lead with SE methods";
		category = "Automated";
		author = "Alban";
	}

	/*@Test(invocationCount = 2)
	public void EditLead() throws InterruptedException 
    {
    	//Click Leads link
    	locateElement("linkText", "Leads").click();
    	//Click Find leads
    	locateElement("linkText", "Find Leads").click();
    	//Enter first name
    	locateElement("xpath", "(//input [@name = 'firstName'])[3]").sendKeys("Alban");
    	//Click Find leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
    	//Click on first resulting lead
    	Thread.sleep(3000);
        locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();
        //Verify title of the page
    	getTitle();
    	//Click Edit
    	locateElement("xpath", "//a[text() = 'Edit']").click();
    	//Change the company name
    	locateElement("xpath", "(//input [@name = 'companyName'])[2]").clear();
       	locateElement("xpath", "(//input [@name = 'companyName'])[2]").sendKeys("DXC Technology");
    	//Click Update
    	locateElement("xpath", "//input [@name = 'submitButton']").click();
    	//Confirm the changed name appears
    	String text = locateElement("xpath", "//span [@id = 'viewLead_companyName_sp']").getText();
    	reportStep("The data: "+text+" entered successfully","Pass");
    	*/
    	        	
    
	@Test(dependsOnMethods = {"Week4.Day2.DeleteLeadSE.DeleteLead"})
	
		public void EditLead1() 
	{
		System.out.println("EditLead");
	}
	/*public void DeleteLead() throws InterruptedException 
    {
    	//Click Leads link
    	locateElement("linkText", "Leads").click();
    	//Click Find leads
    	locateElement("linkText", "Find Leads").click();
       	//Click on Email
    	locateElement("xpath", "//span [text() = 'Email']").click();
    	        	
    	//Enter Email
    	locateElement("xpath", "//input [@name = 'emailAddress']").clear();
    	locateElement("xpath", "//input [@name = 'emailAddress']").sendKeys("al123@dxc.com");
    	   	
     	//Click find leads button
    	locateElement("xpath", "//button [text() = 'Find Leads']").click();
        //Capture lead ID of First Resulting lead
    	Thread.sleep(3000);
        String text2 = locateElement("xpath", "(//a [@class = 'linktext'])[4]").getText();
        reportStep("The data: "+text2+" is clicked successfully","Pass");
        //Click First Resulting lead
        locateElement("xpath", "(//a [@class = 'linktext'])[4]").click();            
        //Click Duplicate Lead
        Thread.sleep(3000);
        locateElement("xpath", "//a[text() = 'Duplicate Lead']").click();	
        //Verify the title as 'Duplicate Lead'
        getTitle();
        //Click Create Lead
    	locateElement("xpath", "//input [@name = 'submitButton']").click();
    	//Confirm the duplicated lead name is same as captured name
    	String text = locateElement("xpath", "//span [@id = 'viewLead_companyName_sp']").getText();
    	reportStep("The data: "+text+" duplicated successfully","Pass");
    	   	
    	    	            
       
    }
*/
    
}
