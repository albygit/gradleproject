package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethodsOld.SeMethods;

public class TC002_CreateLead extends SeMethods
{
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement elelink = locateElement("linktext","CRM/SFA");
		click(elelink);
		WebElement elecreatelead = locateElement("linktext","Create Lead");
		click(elecreatelead);
		WebElement elecompanyname = locateElement("id","createLeadForm_companyName");
		elecompanyname.sendKeys("DXC Technology");
		WebElement elefirstname = locateElement("id","createLeadForm_firstName");
		elefirstname.sendKeys("Alban");
		WebElement elelastname = locateElement("id","createLeadForm_lastName");
		elelastname.sendKeys("Inbaraj");
		WebElement elesubmit = locateElement("name","submitButton");
		elesubmit.click();
		//elesubmit.getText();
		
				
	}
}
