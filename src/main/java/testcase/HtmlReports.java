package testcase;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HtmlReports {

	public static void main(String[] args) throws IOException
	{
		//creating html reports 
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);//apending appeneding the testcases in the reorts true/false 
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//testcase level
		ExtentTest test = extent.createTest("TC001_CreateLead","Create a new lead");
		test.assignCategory("System test");
		test.assignAuthor("Alban");
		
		//testcase step level
		test.pass("Browser launched successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The Data Demomanger entered successfuly", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("The Data crmsfa entered successfuly", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.fail("The element clicked succesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
	    extent.flush();//contains all the html report classes 
	    
	   	
	}

}
