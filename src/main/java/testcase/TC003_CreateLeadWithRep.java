package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class TC003_CreateLeadWithRep extends ProjectMethods
{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new l ead";
		category = "smoke";
		author = "Alban";
	}
		
	
	@Test
	public void createNewLead() {
		//Login is already added to BeforeMethod in ProjectMethods class
		
		WebElement elecreatelead = locateElement("linkText","Create Lead");
		click(elecreatelead);
		WebElement elecompanyname = locateElement("id","createLeadForm_companyName");
		elecompanyname.sendKeys("DXC Technology");
		WebElement elefirstname = locateElement("id","createLeadForm_firstName");
		elefirstname.sendKeys("Alban");
		WebElement elelastname = locateElement("id","createLeadForm_lastName");
		elelastname.sendKeys("Inbaraj");
		WebElement elesubmit = locateElement("name","submitButton");
		elesubmit.click();
		//elesubmit.getText();
		
				
	}
}
